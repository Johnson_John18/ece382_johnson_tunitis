/*--------------------------------------------------------------------
Name: <John Johnson and Daniel Tunitis>
Date: <19 September 2016>
Course: <ECE 382>
File: <main.c>
Event: <Lab 4>

Purp: To learn about using C and assembly code together to create pong
 	  and etch-a-sketch on the LCD peripheral.

Doc:    <None.>

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/
#include <msp430g2553.h>
#include <math.h>
#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include "pong.h"
#include "fonts.h"

// define functions
extern void initMSP();
extern void initLCD();
extern void clearScreen();
extern void Delay160ms();
extern void drawBox(unsigned int col, unsigned int row, unsigned int color);


// set constants
#define		TRUE			1
#define		FALSE			0
#define		UP_BUTTON		(P2IN & BIT2)
#define     RIGHT_BUTTON    (P2IN & BIT3)
#define     LEFT_BUTTON     (P2IN & BIT0)
#define		DOWN_BUTTON     (P2IN & BIT1)
#define     COLOR_BUTTON    (P1IN & BIT3)
#define 	COLOR_WHITE		0xffff
#define		COLOR_BLACK		0x0000
#define		COLOR3			0xee00
#define		COLOR4			0x00ee
#define 	SPEED_DIF		2
#define		MOVE_LEN		10
#define		BALL_SIZE		0x000a
#define		PADDLE_SIZE		0x0014 //twice the size of a ball
#define 	BSTART_X_Pos 	40
#define 	BSTART_Y_Pos 	40
#define 	PSTART_X_Pos 	100
#define 	PSTART_Y_Pos 	250
#define 	BSTART_X_VEL 	10
#define 	BSTART_Y_VEL 	5
#define 	PSTART_X_VEL 	10
#define 	PSTART_Y_VEL 	0
#define 	PBSTART_X_Pos 	4
#define 	PBSTART_Y_Pos 	4
#define 	S1START_X_Pos 	50
#define 	S2START_X_Pos 	110
#define 	S1START_Y_Pos 	50
#define 	S2START_Y_Pos 	50
#define 	UPPERBOUND		0xffff


// draws a smiley face backround for bonus
void drawBackground(){
	drawBox(90, 190, COLOR3);
	drawBox(100, 180, COLOR3);
	drawBox(100, 210, COLOR3);
	drawBox(100, 220, COLOR3);
	drawBox(110, 180, COLOR3);
	drawBox(120, 180, COLOR3);
	drawBox(130, 180, COLOR3);
	drawBox(130, 210, COLOR3);
	drawBox(130, 220, COLOR3);
	drawBox(140, 190, COLOR3);
}


// creates a bouncing ball
void bounceball(){
	// create array of blocks to make circular ball
	ball_t ballArray[9];
	ballArray[0] = createBall(BSTART_X_Pos, BSTART_Y_Pos, BSTART_X_VEL, BSTART_Y_VEL, BALL_SIZE);
	ballArray[1] = createBall(BSTART_X_Pos + 6, BSTART_Y_Pos + 6, BSTART_X_VEL, BSTART_Y_VEL, BALL_SIZE);
	ballArray[2] = createBall(BSTART_X_Pos + 8, BSTART_Y_Pos, BSTART_X_VEL, BSTART_Y_VEL, BALL_SIZE);
	ballArray[3] = createBall(BSTART_X_Pos + 6, BSTART_Y_Pos - 6, BSTART_X_VEL, BSTART_Y_VEL, BALL_SIZE);
	ballArray[4] = createBall(BSTART_X_Pos, BSTART_Y_Pos - 8, BSTART_X_VEL, BSTART_Y_VEL, BALL_SIZE);
	ballArray[5] = createBall(BSTART_X_Pos - 6, BSTART_Y_Pos - 6, BSTART_X_VEL, BSTART_Y_VEL, BALL_SIZE);
	ballArray[6] = createBall(BSTART_X_Pos - 8, BSTART_Y_Pos, BSTART_X_VEL, BSTART_Y_VEL, BALL_SIZE);
	ballArray[7] = createBall(BSTART_X_Pos - 6, BSTART_Y_Pos + 6, BSTART_X_VEL, BSTART_Y_VEL, BALL_SIZE);
	ballArray[8] = createBall(BSTART_X_Pos, BSTART_Y_Pos + 8, BSTART_X_VEL, BSTART_Y_VEL, BALL_SIZE);

	// repeatedly erase the old ball, move it, and draw a new ball
	while(TRUE){
		// erase the old ball
		int j;
		for(j = 0; j < 9; j++){
			drawBox(ballArray[j].position.x, ballArray[j].position.y, COLOR_BLACK);
		}

		// move the center ball which checks for edge collisions
		ballArray[0] = moveCircularBall(ballArray[0], 8);
		// move the rest of the blocks around the center
		ballArray[1] = createBall(ballArray[0].position.x + 6, ballArray[0].position.y + 6, BSTART_X_VEL, BSTART_Y_VEL, BALL_SIZE);
		ballArray[2] = createBall(ballArray[0].position.x + 8, ballArray[0].position.y, BSTART_X_VEL, BSTART_Y_VEL, BALL_SIZE);
		ballArray[3] = createBall(ballArray[0].position.x + 6, ballArray[0].position.y - 6, BSTART_X_VEL, BSTART_Y_VEL, BALL_SIZE);
		ballArray[4] = createBall(ballArray[0].position.x, ballArray[0].position.y - 8, BSTART_X_VEL, BSTART_Y_VEL, BALL_SIZE);
		ballArray[5] = createBall(ballArray[0].position.x - 6, ballArray[0].position.y - 6, BSTART_X_VEL, BSTART_Y_VEL, BALL_SIZE);
		ballArray[6] = createBall(ballArray[0].position.x - 8, ballArray[0].position.y, BSTART_X_VEL, BSTART_Y_VEL, BALL_SIZE);
		ballArray[7] = createBall(ballArray[0].position.x - 6, ballArray[0].position.y + 6, BSTART_X_VEL, BSTART_Y_VEL, BALL_SIZE);
		ballArray[8] = createBall(ballArray[0].position.x, ballArray[0].position.y + 8, BSTART_X_VEL, BSTART_Y_VEL, BALL_SIZE);

		// draw the new ball
		int k;
		for(k = 0; k < 9; k++){
			drawBox(ballArray[k].position.x, ballArray[k].position.y, COLOR_WHITE);
		}
		Delay160ms();
	}

}


// implements the pong class on the LCD peripheral
void pongGame(){
	drawBackground();
	// initialize ball and paddle
	ball_t gameBall = createBall(BSTART_X_Pos, BSTART_Y_Pos, BSTART_X_VEL, 16, BALL_SIZE);
	ball_t paddle = createBall(PSTART_X_Pos, PSTART_Y_Pos, PSTART_X_VEL, PSTART_Y_VEL, PADDLE_SIZE);

	// keep score (starts at 16 because that is the index for 0 in fonts.h
	int score = 0;
	int x;
	int y;
	int a;
	int b;
	int color = COLOR_WHITE;

	// seed the random numbers for color changing
    srand(time(NULL));

	// draw initial ball and paddle
	drawBox(gameBall.position.x, gameBall.position.y, COLOR_WHITE);
	Delay160ms();
	drawBox(paddle.position.x, paddle.position.y, COLOR_WHITE);

	// set up game ending conditions
	int game_over = FALSE;
	while(!game_over){
		// clear the old ball from the screen
		drawBox(gameBall.position.x, gameBall.position.y, COLOR_BLACK);
		drawBox(paddle.position.x, paddle.position.y, COLOR_BLACK);

		// check for ball and paddle collision
		if(gameBall.position.y <= paddle.position.y && gameBall.position.y >= paddle.position.y - BALL_SIZE && gameBall.position.x + gameBall.radius >= paddle.position.x && gameBall.position.x -gameBall.radius <= paddle.position.x + BALL_SIZE){
			// hit the paddle, reverse direction
			gameBall.velocity.y = -gameBall.velocity.y;

			// change ball color for bonus points
			color = (rand() % UPPERBOUND + 1);

			// erase old score
			// first digit
			for(a = 0; a < 5; a++){
				for(b = 0; b < 8; b++){
					if(((1 << b) & font_5x7[((score / 10) + 16)][a]) != 0){
						drawBox(BALL_SIZE * a + S1START_X_Pos, BALL_SIZE * b + S1START_Y_Pos, COLOR_BLACK);
					}
				}
			}
			// second digit
			for(x = 0; x < 5; x++){
				for(y = 0; y < 8; y++){
					if(((1 << y) & font_5x7[((score % 10) + 16)][x]) != 0){
						drawBox(BALL_SIZE * x + S2START_X_Pos, BALL_SIZE * y + S2START_Y_Pos, COLOR_BLACK);
					}
				}
			}
			// add a point to the score
			score += 1;
			// score only goes up to 99... sorry
			if(score > 99){
				score = 0;
			}
		}
		else if(gameBall.position.y - gameBall.radius > paddle.position.y){
			game_over = TRUE; //game is over
		}

		// update the ball's position
		gameBall = moveBall(gameBall);
		// check for button press and move paddle accordingly
		if (RIGHT_BUTTON == FALSE){
			paddle.velocity.x = MOVE_LEN;
			paddle = moveBall(paddle);
		}
		else if(LEFT_BUTTON == FALSE){
			paddle.velocity.x = -MOVE_LEN;
			paddle = moveBall(paddle);
		}
		// change ball speed for bonus functionality
		else if(UP_BUTTON == FALSE){
			// slow down speed
			if(gameBall.velocity.x / SPEED_DIF != 0){
				gameBall.velocity.x /= SPEED_DIF;
			}
			if(gameBall.velocity.y / SPEED_DIF != 0){
				gameBall.velocity.y /= SPEED_DIF;
			}
		}
		else if(DOWN_BUTTON == FALSE){
			// speed up
			if(gameBall.velocity.x * SPEED_DIF > -10 && gameBall.velocity.x * SPEED_DIF < 10){
				gameBall.velocity.x *= SPEED_DIF;
			}
			if(gameBall.velocity.y * SPEED_DIF > -10 && gameBall.velocity.y * SPEED_DIF < 10){
				gameBall.velocity.y *= SPEED_DIF;
			}
		}


		// draw new ball, paddle and score
		drawBackground();
		drawBox(gameBall.position.x, gameBall.position.y, color);
		drawBox(paddle.position.x, paddle.position.y, COLOR_WHITE);

		// first digit
		for(a = 0; a < 5; a++){
			for(b = 0; b < 8; b++){
				if(((1 << b) & font_5x7[((score / 10) + 16)][a]) != 0){
					drawBox(BALL_SIZE * a + S1START_X_Pos, BALL_SIZE * b + S1START_Y_Pos, COLOR4);
				}
			}
		}
		// second digit
		for(x = 0; x < 5; x++){
			for(y = 0; y < 8; y++){
				if(((1 << y) & font_5x7[((score % 10) + 16)][x]) != 0){
					drawBox(BALL_SIZE * x + S2START_X_Pos, BALL_SIZE * y + S2START_Y_Pos, COLOR4);
				}
			}
		}

		Delay160ms();
	}
}


void etchasketch(){
	// set location variables with default locations
	unsigned int color = COLOR_WHITE;
	int x = PBSTART_X_Pos;
	int y = PBSTART_Y_Pos;
	drawBox(x, y, color);
	// seed the random numbers for color changing
    srand(time(NULL));

	while(TRUE) {
		// move in chosen direction based on button press
		if (UP_BUTTON == FALSE){
			y -= MOVE_LEN;
			// check for out of bounds
			if(y <= 0){
				y = 0;
			}
			drawBox(x, y, (rand() % UPPERBOUND + 1));
		}
		else if(DOWN_BUTTON == FALSE){
			y += MOVE_LEN;
			// check for out of bounds
			if(y > (SCREEN_HEIGHT - BALL_SIZE)){
				y = SCREEN_HEIGHT - BALL_SIZE;
			}
			drawBox(x, y, (rand() % UPPERBOUND + 1));
		}
		else if (RIGHT_BUTTON == FALSE){
			x += MOVE_LEN;
			// check for out of bounds
			if(x > (SCREEN_WIDTH - BALL_SIZE)){
				x= SCREEN_WIDTH - BALL_SIZE;
			}
			drawBox(x, y, (rand() % UPPERBOUND + 1));
		}
		else if(LEFT_BUTTON == FALSE){
			x -= MOVE_LEN;
			// check for out of bounds
			if(x < 0){
				x = 0;
			}
			drawBox(x, y, (rand() % UPPERBOUND + 1));
		}
		else if (COLOR_BUTTON == FALSE){
			// invert the color if button is pressed
			if(color == COLOR_BLACK){
				color = COLOR_WHITE;
			}
			else{
				color = COLOR_BLACK;
			}
		}
		Delay160ms();
	}
}


// control all of the functionalities
void main() {
	// === Initialize system ===
	IFG1 = 0; /* clear interrupt flag1 */
	WDTCTL = WDTPW + WDTHOLD; /* stop WD */
	// int button_press = FALSE;

	// prepare LCD for drawing
	initMSP();
	Delay160ms();
	initLCD();
	Delay160ms();
	clearScreen();
	Delay160ms();

	// create a menu for user to select game from
	while(TRUE) {
		// choose game based on button press
		if (UP_BUTTON == FALSE){
			bounceball();
		}
		else if(DOWN_BUTTON == FALSE){
			pongGame();
		}
		else if (RIGHT_BUTTON == FALSE){
			etchasketch();
		}
	}
}

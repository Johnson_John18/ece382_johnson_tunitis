/*--------------------------------------------------------------------
Name: <John Johnson and Daniel Tunitis>
Date: <19 September 2016>
Course: <ECE 382>
File: <pong.h>
Event: <Lab 4>

Purp: To learn about using C and assembly code together to create pong
 	  and etch-a-sketch on the LCD peripheral.

Doc:    <None.>

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/
#ifndef _PONG_H
#define _PONG_H

#define SCREEN_WIDTH 240
#define SCREEN_HEIGHT 320

typedef struct {
    int x;
    int y;
} vector2d_t;

typedef struct {
    vector2d_t position;
    vector2d_t velocity;
    unsigned char radius;
} ball_t;


ball_t createBall(int xPos, int yPos, int xVel, int yVel, unsigned char radius);

vector2d_t initVector(int x, int y);

ball_t moveBall(ball_t ballToMove);

ball_t moveCircularBall(ball_t ballToMove, int offset);

#endif

## Lab 4

### Purpose ###

The purpose of this lab was to better understand the C programming language by creating a program to create an etch-a-sketch type game. This lab will also integrate the LCD into the code in order to visualize the game. It will also require clean, maintainable, modular code that is committed regularly to Git. The lab will also implement the pong game logic from previous assignments.


### Preliminary Design ###

This first chart describes the maximum and minimum values for various data types in C.

| Size | Signed/Unsigned | Type | Min value | Max value |
| :---: | :---: | :---: | :---: | :---: |
| 8-bit | unsigned | unsigned char | 0 | 255 |
| 8-bit | signed | signed char | -128 | 127 |
| 16-bit | unsigned | unsigned short | 0 | 65,535 |
| 16-bit | signed | signed short | -32,768 | 32,767|
| 32-bit | unsigned | unsigned long | 0 | 4,294,967,295 |
| 32-bit | signed | signed long | -2,147,483,648 | 2,147,483,647 |
| 64-bit | unsigned | unsigned long long | 0 | 18,446,744,073,709,551,615 |
| 64-bit | signed | signed long long | -9,223,372,036,854,775,808 | 9,223,372,036,854,775,807 | |

The chart below helps clear the confusion created by the lack of standard data type sizes among C compilers. It also includes the C typedef declaration that would be used to create a data type of a particular size/signedness. 

| Type | Meaning | C typedef declaration |
| :---: | :---: | :---: |
| int8 | unsigned 8-bit value | typedef unsigned char int8; |
| sint8 | signed 8-bit value | typedef signed char sint8; |
| int16 | unsigned 16-bit value | typedef unsigned short int16;|
| sint16 | signed 16-bit value | typedef signed short sint16; |
| int32 | unsigned 32-bit value | typedef unsigned long int32; |
| sint32 | signed 32-bit value | typedef signed long sint32; |
| int64 | unsigned 64-bit value | typedef unsigned long long int64; |
| sint64 | signed 64-bit value | typedef signed long long sint64; | |

This chart shows the values of each variable after each iteration of the while loop in simpleLab4.c.

| Iteration | a | b | c | d | e |
| :---: | :---: | :---: | :---: | :---:| :---: |
| 1st | 2 | 2 | 3 | 4 | 2 | 
| 2nd | 8 | 9 | 8 | 7 | 8 | 
| 3rd | 14 | 15 | 14 | 13 | 14 | 
| 4th | 20 | 21 | 20 | 19 | 20 | 
| 5th | 26 | 27 | 26 | 25 | 26 |  |

The chart below shows the addresses and registers of various values in the simpleLab4.c program.

| Parameter | Value Sought |
| :---: | :---: |
| Starting address of `func` | 0xc044 |
| Ending address of `func` | 0xc050 |
| Register holding w | R12 |
| Register holding x | R13 |
| Register holding y | R14 |
| Register holding z | R15 |
| Register holding return value | R12 | |

### Cross language build constructs

Answer the following questions:

What is the role of the `extern` directive in a .c file?  Hint: check out the [external variable](http://en.wikipedia.org/wiki/External_variable) Wikipedia page.

The `extern` directive tells the compiler that the variable is an external variable. This means that it is available to other functions and is not limited to its own function block. It also means that the variable will be declared during compile time, even if it is not defined.

What is the role of the `.global` directive in an .asm file (used in lines 60-64)?  Hint: reference section 2.6.2 in the MSP 430 Assembly Language Tools v4.3 User's Guide.

The `.global` directive tells the compiler that the variable is a global variable. This means that the variable is visible to other object modules outside of the one that it is defined within, allowing it to be referenced in multiple files.

### Flow Chart ###

Pseudocode:

Required:
Chose starting coords. 
Draw a box

if down pressed: increase y coord, draw new box
if up pressed: decrease y coord, draw new box
if left pressed: decrease x coord, draw new box
if right pressed: increase x coord, draw new box
if color button pressed: change color

B:
draw a box at starting location with a certain speed in both x and y directions

wait

draw over box with box of background color
call movebox
draw new box location
repeat indefintely

A:

Draw a box at starting location for ball and another location for the paddle

call movebox for ball

if left or right pressed move the paddle left or right

draw over ball and paddle if applicable

draw new ball and paddle if applicable

check if the ball hits the paddle

if true change its direction

check if its past the paddle

if true game over




### Debugging ###

When first implementing the required functionality I could not get the screen to run. Capt Falkinburg pointed out that this was because I was not calling my delay subroutine from previous labs. 


###	Testing Methodology ###

Required functionality: https://drive.google.com/open?id=0B-J7aG3qwxX3T2hVSDlac0U4Vlk

B Functionality and Bouncing Ball: https://drive.google.com/open?id=0B-J7aG3qwxX3elU2UF9LZ1NBNDQ

A Functionality and Scoreboard + Background: https://drive.google.com/open?id=0B-J7aG3qwxX3YThBbFpSeC1sUEE

We started the lab by adding the given assembly, pong header, pong source, and blank main files. The main file to edit was main.c. First, we first needed to figure out how to call assembly functions from C. Once we were able to do this, we created a main function to control the three games that were part of this lab. These were pong, etch-a-sketch, and bouncing ball. We then created the bouncing ball function that drew a square that moved across the screen, bouncing off of walls upon contact. Eventually, we added more blocks and a new bounds checking function to create an actual ball on the screen. Next we created the etch-a-sketch function. This function also relied upon drawing blocks and bounds checking. We added the additional functionality that it changed colors with each new block. The final function was pong. This game kept track of the ball's position and the paddle's position in order to check for collisions. A collision would cause the ball to bounce away from the paddle. If the ball moved too far past the paddle, the game would end.

We also drew a background for bonus functionality, which was a smiley face. We also created a start menu that allowed the user to choose the game they wanted to play. We also updated the pong game to change speed with specific button presses.

All of our functionality checking came from direct observation. We were able to see the results of our program by observing the games on the LCD.

### Observations and Conclusions ###

A great deal was learned about calling assembly functions from C. The lab was a good exercise in C programming with the MSP340. It also showed how you can use shorter delays to make a smoother looking game. 

### Documentation Statement ###

EI with Capt Falkinburg
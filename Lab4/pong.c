/*--------------------------------------------------------------------
Name: <John Johnson and Daniel Tunitis>
Date: <19 September 2016>
Course: <ECE 382>
File: <pong.c>
Event: <Lab 4>

Purp: To learn about using C and assembly code together to create pong
 	  and etch-a-sketch on the LCD peripheral.

Doc:    <None.>

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/
#include "pong.h"
#define 	TRUE 	1
#define 	FALSE 	0


// places initial values into a ball_t struct
ball_t createBall(int xPos, int yPos, int xVel, int yVel, unsigned char radius){
	ball_t ball;

	ball.position = initVector(xPos, yPos);
	ball.velocity = initVector(xVel, yVel);
	ball.radius = radius;

	return ball;
}


// helper method for initializing ball_t struct
vector2d_t initVector(int x, int y){
    vector2d_t newVector;

    newVector.x = x;
    newVector.y = y;

    return newVector;
}


// 4 methods for checking if ball goes out of bounds
char rightCheck(ball_t ballToMove){
	if(ballToMove.position.x + ballToMove.velocity.x + ballToMove.radius > SCREEN_WIDTH) return TRUE;
	else return FALSE;
}


char leftCheck(ball_t ballToMove){
	if(ballToMove.position.x + ballToMove.velocity.x - ballToMove.radius < 0) return TRUE;
	else return FALSE;
}


char ceilingCheck(ball_t ballToMove){
	if(ballToMove.position.y + ballToMove.velocity.y - ballToMove.radius < 0) return TRUE;
	else return FALSE;
}


char floorCheck(ball_t ballToMove){
	if(ballToMove.position.y + ballToMove.velocity.y + ballToMove.radius > SCREEN_HEIGHT) return TRUE;
	else return FALSE;
}


//moves the ball location, accounts for boundaries
ball_t moveBall(ball_t ballToMove){
	//check if its hit a horizaontal wall and correct velocity
	if(rightCheck(ballToMove) || leftCheck(ballToMove)){
		ballToMove.velocity.x = -ballToMove.velocity.x; //reverse velocity
	}

	//check if its gonna hit a vertical wall and correct velocity
	if(floorCheck(ballToMove)|| ceilingCheck(ballToMove)){
		ballToMove.velocity.y = -ballToMove.velocity.y; //reverse velocity
	}

	ballToMove.position.y += ballToMove.velocity.y;
	ballToMove.position.x += ballToMove.velocity.x;
	return ballToMove;
}


//moves the circular ball location, accounts for boundaries
ball_t moveCircularBall(ball_t ballToMove, int offset){

	ballToMove.radius += offset;

	//check if its hit a horizaontal wall and correct velocity
	if(rightCheck(ballToMove)){
		ballToMove.velocity.x = -ballToMove.velocity.x; //reverse velocity
	}

	//check if its gonna hit a vertical wall and correct velocity
	if(floorCheck(ballToMove)){
		ballToMove.velocity.y = -ballToMove.velocity.y; //reverse velocity
	}

	ballToMove.radius -= offset;

	if(ceilingCheck(ballToMove)){
		ballToMove.velocity.y = -ballToMove.velocity.y; //reverse velocity
	}

	if(leftCheck(ballToMove)){
		ballToMove.velocity.x = -ballToMove.velocity.x; //reverse velocity
	}

	ballToMove.position.y += ballToMove.velocity.y;
	ballToMove.position.x += ballToMove.velocity.x;

	return ballToMove;
}

/*--------------------------------------------------------------------
Name: <John Johnson and Daniel Tunitis>
Date: <6 November 2016>
Course: <ECE 382>
File: <start5.c>
Event: <Lab 5>

Purp: To decode signals from a remote controller and perform functions on
	  the MSP430 device.

Doc:    <None.>

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/
#include <msp430g2553.h>
#include "start5.h"

int8	newIrPacket = FALSE;
int16	packetData[48];
int8	packetIndex = 0;
int32   irPacket=0;
int8    started = 0;

#define B_ONE 0x61A000FF
#define B_TWO 0x61A0807F
#define B_THREE 0x61A040BF

// -----------------------------------------------------------------------
// -----------------------------------------------------------------------
void main(void) {
	// Set up MSP to process IR and buttons
	initMSP430();

	// create a variable to ensure that only the first pulse of a press is recorded
	int second = FALSE;

	while(1)  {
		// If a new bit is read
		if(newIrPacket == TRUE){
			newIrPacket = FALSE;				// clear the flag

			if(irPacket == B_ONE){
				if(second == FALSE){
					P1OUT ^= BIT0;              // toggle LED 1
					second = TRUE;				// set pulse checker
				}
				else{
					second = FALSE;				// clear pulse checker
				}
			}
			else if(irPacket == B_TWO){
				if(second == FALSE){
					P1OUT ^= BIT6;              // toggle LED 2
					second = TRUE;				// set pulse checker
				}
				else{
					second = FALSE;				// clear pulse checker
				}
			}
			else if(irPacket == B_THREE){
				if(second == FALSE){
					P1OUT ^= BIT0|BIT6;         // toggle both LEDs
					second = TRUE;				// set pulse checker
				}
				else{
					second = FALSE;				// clear pulse checker
				}
			}
		}
	}
} // end main


// -----------------------------------------------------------------------
// In order to decode IR packets, the MSP430 needs to be configured to
// tell time and generate interrupts on positive going edges.  The
// edge sensitivity is used to detect the first incoming IR packet.
// The P2.6 pin change ISR will then toggle the edge sensitivity of
// the interrupt in order to measure the times of the high and low
// pulses arriving from the IR decoder.
//
// The timer must be enabled so that we can tell how long the pulses
// last.  In some degenerate cases, we will need to generate a interrupt
// when the timer rolls over.  This will indicate the end of a packet
// and will be used to alert main that we have a new packet.
// -----------------------------------------------------------------------
void initMSP430() {

	WDTCTL=WDTPW+WDTHOLD; 					// stop WD

	BCSCTL1 = CALBC1_8MHZ;
	DCOCTL = CALDCO_8MHZ;

	P2DIR &= ~BIT6;                     	// Set up P2.6 as GPIO not XIN
	P2SEL &= ~BIT6;							// This action takes
	P2SEL2 &= ~BIT6;						// three lines of code.

	P2IFG &= ~BIT6;							// Clear any interrupt flag on P2.3
	P2IE  |= BIT6;							// Enable P2.3 interrupt

	HIGH_2_LOW;								// check the header out.  P2IES changed.
	P1DIR |= BIT0|BIT6;						// Set LEDs as outputs
	P1OUT &= ~(BIT6|BIT0);					// And turn the LEDs off

	TA0R = 0;
	TACCR0 = 0x3E80;						// 16 ms

	// create a 16ms roll-over period
	TA0CTL &= ~TAIFG;						// clear flag before enabling interrupts = good practice
	TA0CTL = ID_3 | TASSEL_2;				// Use 1:8 prescalar off SMCLK and enable interrupts

	// enable interrupts
	_enable_interrupt();
}

// -----------------------------------------------------------------------
// Since the IR decoder is connected to P2.6, we want an interrupt
// to occur every time that the pin changes - this will occur on
// a positive edge and a negative edge.
//
// Negative Edge:
// The negative edge is associated with end of the logic 1 half-bit and
// the start of the logic 0 half of the bit.  The timer contains the
// duration of the logic 1 pulse, so we'll pull that out, process it
// and store the bit in the global irPacket variable. Going forward there
// is really nothing interesting that happens in this period, because all
// the logic 0 half-bits have the same period.  So we will turn off
// the timer interrupts and wait for the next (positive) edge on P2.6
//
// Positive Edge:
// The positive edge is associated with the end of the logic 0 half-bit
// and the start of the logic 1 half-bit.  There is nothing to do in
// terms of the logic 0 half bit because it does not encode any useful
// information.  On the other hand, we going into the logic 1 half of the bit
// and the portion which determines the bit value, the start of the
// packet, or if the timer rolls over, the end of the ir packet.
// Since the duration of this half-bit determines the outcome
// we will turn on the timer and its associated interrupt.
// -----------------------------------------------------------------------
#pragma vector = PORT2_VECTOR					// This is from the MSP430G2553.h file

__interrupt void pinChange (void) {
	int8	pin;
	int16	pulseDuration;						// The timer is 16-bits

	if (IR_PIN)	pin=1;	else pin=0;

	// read the current pin level
	switch (pin) {
		// !!!!!!!!!NEGATIVE EDGE!!!!!!!!!!
		case 0:
			pulseDuration = TA0R;

			//classify logic 1 half pulse and shift bit into irpacket
			if(pulseDuration < maxLogic1Pulse && pulseDuration > minLogic1Pulse){
				irPacket = irPacket << 1;
				irPacket++;
			}
			else{
				irPacket = irPacket << 1;
			}
			TA0CTL &= ~MC_1;      				// turn off timer a
			LOW_2_HIGH; 						// set up pin interrupt on positive edge
			break;

		// !!!!!!!!POSITIVE EDGE!!!!!!!!!!!
		case 1:
			TA0R = 0x0000;						// time measurements are based at time 0
			TA0CTL |= MC_1;						// turn on timer a
			TACTL |= TAIE;						// turn on interrupt
			HIGH_2_LOW; 						// set up pin interrupt on falling edge
			break;
	} // end switch

	P2IFG &= ~BIT6;								// clear the interrupt flag to prevent immediate ISR re-entry

} // end pinChange ISR

// This is from the MSP430G2553.h file
#pragma vector = TIMER0_A1_VECTOR
__interrupt void timerOverflow (void) {
	TA0CTL &= ~MC_1; 							// turn off timer a
	TACTL &= ~TAIE; 							// turn off interrupt
	newIrPacket = TRUE;							// set newPacket flag
	TA0CTL &= ~TAIFG; 							// clear taifg
}
